package net.zeroyear.murderstat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class MurderListener implements Listener {
    MySQL db;

    MurderListener(MySQL database){
        db = database;
    }

    @EventHandler
    public void onMobKill(EntityDeathEvent e){
        if(e.getEntity().getKiller() != null && e.getEntity() instanceof Player){
            db.addPlayerKill(e.getEntity().getKiller(), ((Player) e.getEntity()));
        }else if(e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent){
            db.addMobKill(((EntityDamageByEntityEvent) e.getEntity().getLastDamageCause()).getDamager(), e.getEntity());
        }
    }
}
