package net.zeroyear.murderstat;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public final class MurderStat extends JavaPlugin {
    FileConfiguration config;
    Logger log;
    MySQL db;
    MurderListener listener;

    @Override
    public void onEnable(){
        config = getConfig();
        log = getServer().getLogger();
        this.saveDefaultConfig(); // Save the default configuration file if it already exists

        // Open connection to database
        db = new MySQL(this);

        // Start the event listener and send the DB object as a parameter.
        listener = new MurderListener(db);
        getServer().getPluginManager().registerEvents(listener, this);
    }

    @Override
    public void onDisable(){

    }
}