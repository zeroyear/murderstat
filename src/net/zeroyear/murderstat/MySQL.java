package net.zeroyear.murderstat;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;


public class MySQL {
    private MurderStat plugin;
    private Logger log;

    private String uri;
    private String mktable, pktable;

    private Connection conn = null;
    private Statement statement = null;

    MySQL(MurderStat instance) {
        plugin = instance;
        log = plugin.getLogger();

        uri = "jdbc:mysql://" + instance.getConfig().getString("database.host") + ":" + instance.getConfig().getString("database.port") + "/" + instance.getConfig().getString("database.db");
        mktable = instance.getConfig().getString("database.mk-table");
        pktable = instance.getConfig().getString("database.pk-table");
        try{
            conn = DriverManager.getConnection(uri, instance.getConfig().getString("database.user"), instance.getConfig().getString("database.pass"));
            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `" + mktable + "` (" +
                    "  `id` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT," +
                    "  `player` VARCHAR(32) NOT NULL," +
                    "  `mob` ENUM(  'Bat',  'Chicken',  'Cow',  'MushroomCow',  'Ozelot',  'Pig',  'Sheep',  'Squid',  'Villager',  'Enderman',  'Wolf',  'PigZombie',  'Blaze',  'CaveSpider',  'Creeper',  'Ghast',  'LavaSlime',  'Silverfish',  'Skeleton',  'Slime',  'Spider', 'Witch',  'Zombie',  'SnowMan',  'VillagerGolem',  'EnderDragon',  'WitherBoss' ) NOT NULL," +
                    "  `winner` ENUM('player','mob') NOT NULL," +
                    "  PRIMARY KEY (`id`)," +
                    "  KEY `player` (`player`,`mob`)" +
                    ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");

            statement = conn.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `" + pktable + "` (" +
                    "  `id` INT(32) UNSIGNED NOT NULL AUTO_INCREMENT," +
                    "  `killer` VARCHAR(32) NOT NULL," +
                    "  `victim` VARCHAR(32) NOT NULL," +
                    "  PRIMARY KEY (`id`)," +
                    "  KEY `player` (`killer`,`victim`)" +
                    ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
        }catch(SQLException e){
            logErr(e);
        }
    }
    public void addMobKill(Entity attacker, Entity victim){
        String winner;
        String playerName;
        String mobName;

        if(attacker instanceof Player || victim instanceof Player && attacker != null){
            if(attacker instanceof Player){
                winner = "player";
                playerName = ((Player) attacker).getName();
                mobName = victim.getType().getName();
            }else{
                winner = "mob";
                playerName = ((Player) victim).getName();
                mobName = attacker.getType().getName();
            }

            try{
                statement = conn.createStatement();
                statement.executeUpdate("INSERT INTO `" + mktable + "`(`player`,`mob`,`winner`) VALUES('" + playerName + "', '" + mobName + "','" + winner + "')");
            }catch(SQLException e){
                logErr(e);
            }
        }else{
            log.severe("Damage cause entity is null");
        }
    }

    public void addPlayerKill(Player attacker, Player victim){
        log.info("Attempting to log PK with query:");
        log.info("INSERT INTO `" + pktable + "`(`killer`,`victim`) VALUES('" + attacker.getName() + "', '" + victim.getName() + "')");
        try{
            statement = conn.createStatement();
            statement.executeUpdate("INSERT INTO `" + pktable + "`(`killer`,`victim`) VALUES('" + attacker.getName() + "', '" + victim.getName() + "')");
        }catch(SQLException e){
            logErr(e);
        }
    }

    private void logErr(SQLException e){
        log.severe("MySQL connection failed with the following error:");
        log.severe(e.getMessage());
        plugin.getPluginLoader().disablePlugin(plugin);
    }
}
